package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import java.util.List;
import ApiInterface.CategoryAPI;
import adapter.BrandWiseListAdapter;
import alertbox.Alertbox;
import home.Home_Activity;
import model.getbrand.GetBrandList;
import ApiInterface.CategoryAPI;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import toast.StyleableToasty;
import ApiInterface.CategoryAPI;


public class BrandSearch extends AppCompatActivity {

    private ListView brandlist;
    private BrandWiseListAdapter brandWiseListAdapter;
    private List<String> brandname;
    private StyleableToasty toast;
    private Alertbox alertbox = new Alertbox(this);
    private ImageView product_detail_back,product_detail_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_search);

        brandlist=findViewById(R.id.brandlist);
        product_detail_back=findViewById(R.id.product_detail_back);
        product_detail_home=findViewById(R.id.product_detail_home);

        product_detail_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(), Home_Activity.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        product_detail_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        checkInternet();
    }

    private void checkInternet() {
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet()){
            getbranlist();
        }
        else
            alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

    private void getbranlist() {
        final ProgressDialog progressDialog = new ProgressDialog(BrandSearch.this,
                R.style.Progress);
        try {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI api = RetroClient.RetroClient.getApiService();
            Call<GetBrandList> call = api.brandlist();
            toast = new StyleableToasty(BrandSearch.this);
            call.enqueue(new Callback<GetBrandList>() {
                @Override
                public void onResponse(Call<GetBrandList> call, Response<GetBrandList> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        brandname=response.body().getData();
                        brandWiseListAdapter =new BrandWiseListAdapter(BrandSearch.this,brandname);
                        brandlist.setAdapter(brandWiseListAdapter);
                    } else if (response.body().getResult().equals("notsuccess")){
                        StyleableToasty styleableToasty=new StyleableToasty(BrandSearch.this);
                        styleableToasty.showFailureToast("No Record Found Try Again Later");
                    }
                    else if(response.body().getResult().equals("error"))
                    {
                        StyleableToasty styleableToasty=new StyleableToasty(BrandSearch.this);
                        styleableToasty.showFailureToast("No Record Found Error Try Again Later");
                    }
                }

                @Override
                public void onFailure(Call<GetBrandList> call, Throwable t) {
                    progressDialog.dismiss();
                    StyleableToasty styleableToasty=new StyleableToasty(BrandSearch.this);
                    styleableToasty.showFailureToast("Something went Wrong Failure. Please try again later...!");
                }
            });
        }
        catch (Exception r)
        {
            progressDialog.dismiss();
            StyleableToasty styleableToasty=new StyleableToasty(BrandSearch.this);
            styleableToasty.showFailureToast("Something went Wrong Expection. Please try again later...!");
        }

    }

}
