package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ServiceAdapter;
import adapter.ServiceAdapterTwo;
import alertbox.Alertbox;
import home.Home_Activity;
import model.FullUnitSearch;
import model.FullUnitSearchList;
import model.PartSearch.ChildPartsList;
import model.PartSearch.ServicePartsList;
import model.SearchFullUnit;
import model.SearchFullUnitList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetails extends AppCompatActivity {
    private String partNumber;
    private ListView list;
    private List<ServicePartsList> servicePartsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details);
//        partNumber=getIntent().getStringExtra("Partnumber");
        servicePartsList= (List<ServicePartsList>) getIntent().getSerializableExtra("Partnumber");
        servicePartsList.remove(null);

        ImageView back = findViewById(R.id.service_back);
        ImageView home = findViewById(R.id.service_home);
        ImageView cart = findViewById(R.id.service_cart_details);



        list = findViewById(R.id.service_list);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ServiceDetails.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ServiceDetails.this,OrderActivity.class));
            }
        });
        checkInternet();

    }

    private void checkInternet() {
        NetworkConnection net = new NetworkConnection(ServiceDetails.this);
        if (net.CheckInternet()) {
//            getpartdetails();
            getService();
        } else {
            Toast.makeText(this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getpartdetails()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ServiceDetails.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {

            final CategoryAPI service = RetroClient.getApiService();
            Call<FullUnitSearch> call = service.fullUnitSearch(partNumber);

            call.enqueue(new Callback<FullUnitSearch>() {
                @Override
                public void onResponse(Call<FullUnitSearch> call, Response<FullUnitSearch> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        List<FullUnitSearchList> partList = response.body().getData();
//                    ServiceAdapter serviceAdapter = new ServiceAdapter(ServiceDetails.this, childPartsList);
//                    list.setAdapter(serviceAdapter);
                    } else if (response.body().getResult().equals("ServiceSuccess")) {
                        progressDialog.dismiss();
                        getServiceParts();
                    } else {
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                ServiceDetails.this).create();

                        LayoutInflater inflater = (ServiceDetails.this).getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                        alertDialog.setView(dialogView);
                        Button Ok = (Button) dialogView.findViewById(R.id.ok);
                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                    final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                        Message.setText("No Parts Available");
                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(ServiceDetails.this, ProductCatalogue.class));
                                alertDialog.dismiss();
                            }
                        });

//                    send.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            startActivity(new Intent(ServiceDetails.this, SendEnquiry.class));
//                            alertDialog.dismiss();
//                        }
//                    });
                        alertDialog.show();
                        progressDialog.dismiss();
                        Toast.makeText(ServiceDetails.this, "No Record Found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<FullUnitSearch> call, Throwable t) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                            ServiceDetails.this).create();

                    LayoutInflater inflater = (ServiceDetails.this).getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                    alertDialog.setView(dialogView);
                    Button Ok = (Button) dialogView.findViewById(R.id.ok);
                    final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                    Message.setText("Something went Wrong !");
                    Ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(ServiceDetails.this, ProductCatalogue.class));
                            alertDialog.dismiss();
                        }
                    });
//                send.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(ServiceDetails.this, SendEnquiry.class));
//                        alertDialog.dismiss();
//                    }
//                });
                    alertDialog.show();
                    Toast.makeText(getApplicationContext(), "Something went Wrong", Toast.LENGTH_LONG).show();

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //get the service parts
    public void getServiceParts()
    {
        CategoryAPI service = RetroClient.getApiService();

        Call<SearchFullUnit> call=service.searchFullUnit(partNumber);
        call.enqueue(new Callback<SearchFullUnit>() {
            @Override
            public void onResponse(Call<SearchFullUnit> call, Response<SearchFullUnit> response) {
                if(response.body().getResult().equals("ServiceSuccess"))
                {
                    List<SearchFullUnitList> partList=response.body().getData();
//                    ServiceAdapterTwo serviceAdapter = new ServiceAdapterTwo(ServiceDetails.this, partList);
//                    list.setAdapter(serviceAdapter);
                }
            }

            @Override
            public void onFailure(Call<SearchFullUnit> call, Throwable t) {

                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                        ServiceDetails.this).create();

                LayoutInflater inflater = (ServiceDetails.this).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                alertDialog.setView(dialogView);
                Button Ok = (Button) dialogView.findViewById(R.id.ok);
                final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                Message.setText("Something went Wrong !");
                Ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(ServiceDetails.this, ProductCatalogue.class));
                        alertDialog.dismiss();
                    }
                });
//                send.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(ServiceDetails.this, SendEnquiry.class));
//                        alertDialog.dismiss();
//                    }
//                });
                alertDialog.show();
                Toast.makeText(getApplicationContext(), "Something went Wrong", Toast.LENGTH_LONG).show();
            }
        });


    }

    //pass the service details as a list to pass in adapter
    public void getService()
    {
        ServiceAdapterTwo serviceAdapter = new ServiceAdapterTwo(ServiceDetails.this, servicePartsList);
        list.setAdapter(serviceAdapter);
    }
}
