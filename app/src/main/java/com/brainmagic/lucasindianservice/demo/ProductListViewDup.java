package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ProductDetailsAdapter;
import alertbox.Alertbox;
import home.Home_Activity;
import model.ProductSearch;
import model.ProductSearchList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListViewDup extends AppCompatActivity {


    private ListView productList;
    private String product;
    private NetworkConnection net;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_view);

        net = new NetworkConnection(ProductListViewDup.this);
        ImageView backButton = findViewById(R.id.product_list_view_back);
        ImageView home = findViewById(R.id.product_list_view_home);
        final ImageView cart = findViewById(R.id.products_cart_details);
        TextView details = findViewById(R.id.details);
        TextView detailsList = findViewById(R.id.product_detail_list);
        TextView productTypes=findViewById(R.id.product_type);
        Intent intent = getIntent();
        String productType=intent.getExtras().getString("ProductType");
        product = intent.getExtras().getString("Product");
//        String rating = intent.getExtras().getString("Rating");
//        String voltage = intent.getExtras().getString("Voltage");
//        String type = intent.getExtras().getString("Type");
        productList = findViewById(R.id.product_details);
        details.setText(product);
        productTypes.setText(productType);
        detailsList.setText(productType);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductListViewDup.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductListViewDup.this,OrderActivity.class));
            }
        });

        checkInternet();
    }

    public void checkInternet()
    {
        if(net.CheckInternet())
        {
            getProductdetails();
        }
        else {
            Alertbox alertbox=new Alertbox(ProductListViewDup.this);
            alertbox.showNegativebox("Please check your network connection and try again!");
//            Toast.makeText(this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }
    public void getProductdetails()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ProductListViewDup.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        CategoryAPI service = RetroClient.getApiService();

        try {
            Call<model.ProductSearch> call = service.productSearchNew(product);
            call.enqueue(new Callback<model.ProductSearch>() {
                @Override
                public void onResponse(Call<model.ProductSearch> call, Response<model.ProductSearch> response) {
                    if (response.body().getResult().equals("success")) {
                        List<ProductSearchList> Producfilter = response.body().getData();
                        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(ProductListViewDup.this, Producfilter);
                        productList.setAdapter(productDetailsAdapter);
                        progressDialog.dismiss();
                    }else if (response.body().getResult().equals("notsuccess")){
                        if(response.body().getResult()==null)
                        {
                            Alertbox alertbox=new Alertbox(ProductListViewDup.this);
                            alertbox.showNegativebox("No Details found for "+product);
                        }
                        else {
                            Alertbox alertbox=new Alertbox(ProductListViewDup.this);
                            alertbox.showNegativebox("No Details found for "+product);
                        }
//                        Toast.makeText(getApplicationContext(), "No FeedBackData Found", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                    else {
                        progressDialog.dismiss();
                        Alertbox alertbox=new Alertbox(ProductListViewDup.this);
                        alertbox.showNegativebox("Invalid Search. Please try after some time");

                    }
                }
                @Override
                public void onFailure(Call<ProductSearch> call, Throwable t) {
                    Alertbox alertbox=new Alertbox(ProductListViewDup.this);
                    alertbox.showNegativebox("No Record Found");
//                    Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
