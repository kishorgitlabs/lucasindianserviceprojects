package com.brainmagic.lucasindianservice.demo;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import alertbox.Alertbox;
import home.Home_Activity;
import model.feedback.FeedBack;
import model.uploadimage.UploadImage;
import network.NetworkConnection;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class FeedBackForm extends AppCompatActivity  {


    private MaterialSpinner feedbacktype;
    private ArrayList<String> feedbacktypes;
    private EditText feedbackgiven;
    private Button feedbacksubmit;
    private String selectedfeedbacktype="",feedbackgivenbyuser,username,usertype,usermobile,User_EmailId;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private ProgressDialog loading;
    private ImagePicker imagePicker;
    private String mImageName = "";
    private File mImageFile;
    String imagename="";
    private Uri i;
    private Button addimage;
    private ImageView selectedimage,product_back,product_home;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back_form);
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);

        username= myshare.getString("UserName", "");
        usertype= myshare.getString("UserType", "");
        usermobile= myshare.getString("Mobile", "");
        feedbacktype = (MaterialSpinner) findViewById(R.id.feedbacktype);
        feedbackgiven=findViewById(R.id.feedbackgiven);
        addimage=findViewById(R.id.addimage);
        selectedimage=findViewById(R.id.selectedimage);
        product_home=findViewById(R.id.product_home);
        product_back=findViewById(R.id.product_back);
        feedbacksubmit=findViewById(R.id.feedbacksubmit);
        feedbacktype.setBackgroundResource(R.drawable.background_spinner);
        feedbacktypes = new ArrayList<>();
        feedbacktypes.add("Select Complaint Type");
        feedbacktypes.add("Feedback");
        feedbacktypes.add("Complaint");
        feedbacktypes.add("Issue");
        feedbacktype.setItems(feedbacktypes);

        product_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        product_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FeedBackForm.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

         feedbacktype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner materialSpinner, int i, long l, Object o) {
                selectedfeedbacktype=o.toString();
            }
        });
        if (ContextCompat.checkSelfPermission(FeedBackForm.this, Manifest.permission.GET_ACCOUNTS)== PackageManager.PERMISSION_GRANTED){
            User_EmailId = getEmiailID(getApplicationContext());
        }else {
            ActivityCompat.requestPermissions(FeedBackForm.this,new String[]{Manifest.permission.GET_ACCOUNTS},9);
        }
        feedbacksubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedbackgivenbyuser=feedbackgiven.getText().toString();
                if (selectedfeedbacktype.equals("")||selectedfeedbacktype.equals("Select Complaint Type")){
                    StyleableToasty styleableToasty = new StyleableToasty(FeedBackForm.this);
                    styleableToasty.showSuccessToast("Please select complaint type");
                }else if (TextUtils.isEmpty(feedbackgivenbyuser)){
                    StyleableToasty styleableToasty = new StyleableToasty(FeedBackForm.this);
                    styleableToasty.showSuccessToast("Feedback cannot be empty");
                }else {
                    checkInternet();
                }
            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePicker=new ImagePicker(FeedBackForm.this, null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        selectedimage.setImageURI(imageUri);
                        mImageName = getFileName(imageUri);
                        mImageFile = getFileFromImage();
                        i=imageUri;
                    }
                });
                imagePicker.choosePicture(true);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (requestCode==200){
            super.onActivityResult(requestCode, resultCode, data);
//            if (imagePicker.handleActivityResult(resultCode,requestCode,data!=null)) {
            if (resultCode == RESULT_OK) {
                imagePicker.handleActivityResult(resultCode, requestCode, data);

            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==9&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
            User_EmailId = getEmiailID(getApplicationContext());
        }else {
            finish();
            StyleableToasty styleableToasty = new StyleableToasty(FeedBackForm.this);
            styleableToasty.showSuccessToast("Please Provide Premission For Send Feedbcak");
        }
    }

    private void checkInternet() {
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            if (i==null){
                mImageName="noimage.png";
                sendfeedback();
            }else {
                uploadimage(mImageFile);
            }

        else
            alertnew(getResources().getString(R.string.no_internet));
    }

    private void sendfeedback() {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
           CategoryAPI service=RetroClient.getApiService();
            Call<FeedBack> call = service.feedback(username,usermobile,User_EmailId,selectedfeedbacktype,usertype,feedbackgivenbyuser,mImageName);
            call.enqueue(new Callback<FeedBack>() {
                @Override
                public void onResponse(Call<FeedBack> call, Response<FeedBack> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            alertnewfed("Your Feedback is Important To Us Thank You");
                        } else {
                            loading.dismiss();
                            alertnew("Please Try Again Later");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertnew(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<FeedBack> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    String tt=t.getMessage();
                    alertnewfed(tt+"Please Try Again Later");
//                    String ttt=t.getCause();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            loading.dismiss();
            alertnew("Exception Error Please Try Again Later");
        }
    }

    private void uploadimage(File selectimage) {
        try {
            progressDialog = new ProgressDialog(FeedBackForm.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Sending email...");
            progressDialog.show();

            CategoryAPI service = RetroClient.getApiServiceUpload();
            String type="image/png";
            imagename=selectimage.getName();
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), selectimage);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", selectimage.getName(), requestBody);
            final Call<UploadImage> request = service.uploadimage(filePart);
            request.enqueue(new Callback<UploadImage>() {
                @Override
                public void onResponse(Call<UploadImage> call, Response<UploadImage> response) {
                    if (response.body().getMessage().equals("Success.")) {
                        progressDialog.dismiss();
                        sendfeedback();
//                        uploaduserdetails();
                    } else {
//                        sendfeedback();
                        alertnew("Please Try Again later");
                                }
                }
                @Override
                public void onFailure(Call<UploadImage> call, Throwable t) {
                    Log.v("Upload Exception", t.getMessage());
                    progressDialog.dismiss();
                    t.printStackTrace();
                }

            });
        }
        catch (Exception ex) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            Log.v("Exception", ex.getMessage());

        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }
    private File getFileFromImage() {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectedimage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + mImageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }

    public void alertnew(String s) {

        Alertbox alert = new Alertbox(FeedBackForm.this);
        alert.newalert(s);
    }
  public void alertnewfed(String s) {

        Alertbox alert = new Alertbox(FeedBackForm.this);
        alert.newalertfeedback(s);
    }



     private static String getEmiailID(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }
    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }


//    protected void sendEmail() {
//        Log.i("Send email", "");
//
//        Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setData(Uri.parse("mailto:"));
//        emailIntent.setType("message/rfc822");
//        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"balaji@brainmagic.info"});
//        // emailIntent.putExtra(Intent.EXTRA_CC, CC);
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, selectedfeedbacktype);
//        String msg = " Name         : " + username + " \n Mobile No    : " + usermobile + "\n User Type    : " + usertype
//                + "\n Assistance Required  : " + feedbackgivenbyuser;
//        Log.v("Body", msg);
//
//        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
//
//        try {
//            startActivity(Intent.createChooser(emailIntent, "Sending email..."));
//            finish();
//            Log.i("Finish sending email...", "");
//        } catch (android.content.ActivityNotFoundException ex) {
//            //Toast.makeText(AskWabcoActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
//            alertnew("You don't have a default email configured in your mobile. Cannot attach file !");
//        }
//    }
//
//    public void SendMail() {
//        progressDialog = new ProgressDialog(FeedBackForm.this);
//         progressDialog.setCancelable(false);
//        progressDialog.setMessage("Sending email...");
//        progressDialog.show();
//
//        String msg = " Name         : " + username + " \n Mobile No    : " + usermobile + "\n User Type    : " + usertype
//                + "\n Assistance Required  : " + feedbackgivenbyuser;
//        Log.v("Body", msg);
//        String subject = selectedfeedbacktype;
//        // String mail = "customer.care@wabco-auto.com,sivakumar.s@wabco-auto.com,rkumaravel@brainmagic.info";
//        String mail = "balaji@brainmagic.info";
//        try {
//            BackgroundMail backgroundMail =   BackgroundMail.newBuilder(FeedBackForm.this)
//                    .withUsername(User_EmailId)
//                    .withPassword("Suk37q$8")
//                    .withMailto(mail)
//                    .withType(BackgroundMail.TYPE_PLAIN)
//                    .withSubject(subject)
//                    .withBody(msg)
//                    .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
//                        @Override
//                        public void onSuccess() {
//                            //do some magic
//                            progressDialog.dismiss();
//                            alertnew("Your mail has been sent successfully !");
//                        }
//                    })
//                    .withOnFailCallback(new BackgroundMail.OnFailCallback() {
//                        @Override
//                        public void onFail() {
//                            //do some magic
//                            progressDialog.dismiss();
//                            //alertbox.showAlertbox("Your mail was not sent!");
//                            sendEmail();
//                        }
//                    })
//                    .send();
//
//
//
//        } catch (Exception e) {
//            progressDialog.dismiss();
//            alertnew(e.toString());
//        }


//    }

}
