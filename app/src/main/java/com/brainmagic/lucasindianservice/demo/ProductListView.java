package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.ProductDetailsAdapter;
import alertbox.Alertbox;
import home.Home_Activity;
import model.ProductSearch;
import model.ProductSearchList;
import model.RatingList;
import model.TypeList;
import model.VoltageList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListView extends AppCompatActivity {


    private ListView productList;
    private String product;
    private List<String> voltagespinlist, ratingspinlist, typespinlist;
    private ProgressDialog  loading;
    private NetworkConnection net;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_view);

        net = new NetworkConnection(ProductListView.this);
        ImageView backButton=findViewById(R.id.product_list_view_back);
        ImageView home=findViewById(R.id.product_list_view_home);
        final ImageView cart=findViewById(R.id.products_cart_details);
        TextView details=findViewById(R.id.details);
        TextView detailsList=findViewById(R.id.product_detail_list);
        TextView productTypes=findViewById(R.id.product_type);

        Intent intent=getIntent();
        String productType=intent.getExtras().getString("ProducType");
        product=intent.getExtras().getString("Product");
        String rating=intent.getExtras().getString("Rating");
        String voltage=intent.getExtras().getString("Voltage");
        String type=intent.getExtras().getString("Type");
        productList=findViewById(R.id.product_details);
        details.setText(product);
        productTypes.setText(productType);
        detailsList.setText(productType);

        voltagespinlist = new ArrayList<String>();
        ratingspinlist = new ArrayList<String>();
        typespinlist = new ArrayList<String>();
        voltagespinlist.add("Please select voltage");
        ratingspinlist.add("Please select rating");
        typespinlist.add("Please select type");


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductListView.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductListView.this,OrderActivity.class));
            }
        });

        checkInternet(rating,voltage,type);

//        if(net.CheckInternet()) {
//            if (product_detail.equals("rating")) {
//                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ProductListView.this).create();
//
//                LayoutInflater inflater = (ProductListView.this).getLayoutInflater();
//                View dialog_box = inflater.inflate(R.layout.product_config, null);
//                alertDialog.setView(dialog_box);
//                alertDialog.setCancelable(false);
//                alertDialog.show();
//                final MaterialSpinner voltagespin = (MaterialSpinner) dialog_box.findViewById(R.id.voltagespin);
//                final MaterialSpinner ratingspin = (MaterialSpinner) dialog_box.findViewById(R.id.ratingspin);
//                final MaterialSpinner typespin = (MaterialSpinner) dialog_box.findViewById(R.id.typespin);
//                Button back = dialog_box.findViewById(R.id.cancel_product);
//                Button submit = dialog_box.findViewById(R.id.search_product);
//
//                voltagespin.setVisibility(View.VISIBLE);
//                ratingspin.setVisibility(View.VISIBLE);
//                typespin.setVisibility(View.GONE);
//                voltagespin.setItems(voltagespinlist);
//                voltagespin.setPadding(30, 0, 0, 0);
//                ratingspin.setItems(ratingspinlist);
//                ratingspin.setPadding(30, 0, 0, 0);
//
//                if(net.CheckInternet())
//                {
//                    loading = ProgressDialog.show(ProductListView.this, "Loading Voltage", "Please wait", false, false);
//
//                    try {
//                        CategoryAPI service = RetroClient.getApiService();
//                        Call<VoltageList> call = service.voltageList(product);
//
//                        call.enqueue(new Callback<VoltageList>() {
//                            @Override
//                            public void onResponse(Call<VoltageList> call, Response<VoltageList> response) {
//                                loading.dismiss();
//                                if (response.body().getResult().equals("success")) {
//                                    voltagespinlist = response.body().getData();
//                                    voltagespinlist.add(0, "Please select voltage");
//                                    voltagespin.setItems(voltagespinlist);
//                                    voltagespin.setPadding(30, 0, 0, 0);
//                                } else {
//                                    Toast.makeText(ProductListView.this, "No Voltage Available", Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(Call<VoltageList> call, Throwable t) {
//                                loading.dismiss();
//                                Toast.makeText(ProductListView.this, "No Voltage Available", Toast.LENGTH_SHORT).show();
//
//                            }
//                        });
//                    }catch (Exception e)
//                    {
//                        e.printStackTrace();
//                    }
//
//                    voltagespin.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                        @Override
//                        public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                            if(!item.toString().equals("Please select voltage"))
//                            {
//                                loading = ProgressDialog.show(ProductListView.this, "Loading Rating", "Please wait", false, false);
//                               try {
//                                   CategoryAPI service = RetroClient.getApiService();
//                                   Call<RatingList> call = service.ratingList(product, voltagespin.getText().toString().trim());
//                                   call.enqueue(new Callback<RatingList>() {
//                                       @Override
//                                       public void onResponse(Call<RatingList> call, Response<RatingList> response) {
//                                           loading.dismiss();
//                                           if (response.body().getResult().equals("success")) {
//                                               ratingspinlist = new ArrayList<String>();
//                                               ratingspinlist = response.body().getData();
//                                               ratingspinlist.add(0, "Please select rating");
//                                               ratingspin.setItems(ratingspinlist);
//                                               ratingspin.setPadding(30, 0, 0, 0);
////                                            Rating = ratingspin.getText().toString().trim();
//
//                                           } else {
//                                               ratingspinlist.clear();
//                                               ratingspinlist.add(0, "Please select rating");
//                                               ratingspin.setItems(ratingspinlist);
//                                               ratingspin.setPadding(30, 0, 0, 0);
////                                            Rating = ratingspin.getText().toString().trim();
//                                               Toast.makeText(ProductListView.this, "No Ratings Available", Toast.LENGTH_SHORT).show();
//                                           }
//                                       }
//
//                                       @Override
//                                       public void onFailure(Call<RatingList> call, Throwable t) {
//                                           loading.dismiss();
//                                           Toast.makeText(ProductListView.this, "No Ratings Available", Toast.LENGTH_SHORT).show();
//                                       }
//                                   });
//                               }catch (Exception e)
//                               {
//                                   e.printStackTrace();
//                               }
//                            }
//                            else {
//                                ratingspinlist.clear();
//                                ratingspinlist.add(0, "Please select rating");
//                                ratingspin.setItems(ratingspinlist);
//                                ratingspin.setPadding(30, 0, 0, 0);
////                                Rating = ratingspin.getText().toString().trim();
//                            }
//                        }
//                    });
//
//
//                    submit.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//                            String Rating = ratingspin.getText().toString().trim();
//                            String Voltage = voltagespin.getText().toString().trim();
//
//                            if (Voltage.equals("Please select voltage")) {
//                                Voltage = "";
//                                Toast.makeText(ProductListView.this, "Please select voltage", Toast.LENGTH_SHORT).show();
//                            } else if (Rating.equals("Please select rating")) {
//                                Rating = "";
//                                Toast.makeText(ProductListView.this, "Please select rating", Toast.LENGTH_SHORT).show();
//                            } else {
//                                String Type = "";
//                                alertDialog.dismiss();
//                                checkInternet(Rating,Voltage,Type);
//                            }
//
//
//                        }
//                    });
//                    back.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            onBackPressed();
//                            alertDialog.dismiss();
//
//                        }
//                    });
////                    alertDialog.getWindow().setLayout(600, 600);
//                    alertDialog.show();
//                }
//
//            } else if (product_detail.equals("type")) {
//                final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(ProductListView.this).create();
//                LayoutInflater inflater = (ProductListView.this).getLayoutInflater();
//                View dialog_box = inflater.inflate(R.layout.product_config, null);
//                alertDialog.setCancelable(false);
//                alertDialog.setView(dialog_box);
//                alertDialog.show();
//                final MaterialSpinner voltagespin = (MaterialSpinner) dialog_box.findViewById(R.id.voltagespin);
//                final MaterialSpinner ratingspin = (MaterialSpinner) dialog_box.findViewById(R.id.ratingspin);
//                final MaterialSpinner typespin = (MaterialSpinner) dialog_box.findViewById(R.id.typespin);
//
//                Button back = dialog_box.findViewById(R.id.cancel_product);
//                Button submit = dialog_box.findViewById(R.id.search_product);
//
//                voltagespin.setVisibility(View.GONE);
//                ratingspin.setVisibility(View.GONE);
//                typespin.setVisibility(View.VISIBLE);
//
//                typespin.setItems(typespinlist);
//                typespin.setPadding(30, 0, 0, 0);
//
//                typespin.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (net.CheckInternet()) {
//                            try {
//                                CategoryAPI service = RetroClient.getApiService();
//                                Call<TypeList> call = service.typeList(product);
//                                call.enqueue(new Callback<TypeList>() {
//                                    @Override
//                                    public void onResponse(Call<TypeList> call, Response<TypeList> response) {
//                                        if (response.body().getResult().equals("success")) {
//                                            typespinlist = response.body().getData();
//                                            typespinlist.add(0, "Please select type");
//                                            typespin.setItems(typespinlist);
//                                            typespin.setPadding(30, 0, 0, 0);
//                                        } else {
//                                            typespinlist.clear();
//                                            typespinlist.add(0, "Please select type");
//                                            typespin.setItems(typespinlist);
//                                            typespin.setPadding(30, 0, 0, 0);
//                                            Toast.makeText(ProductListView.this, "No Type Available", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<TypeList> call, Throwable t) {
//                                        typespinlist.clear();
//                                        typespinlist.add(0, "Please select type");
//                                        typespin.setItems(typespinlist);
//                                        typespin.setPadding(30, 0, 0, 0);
//                                        Toast.makeText(ProductListView.this, "No Type Available", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//                        }
//                        else
//                        {
//                            Toast.makeText(getApplicationContext(),"Please Check your Network ",Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//
//                back.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                });
//
////                submit.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////                        Toast.makeText(getApplicationContext(), "adsfsdf", Toast.LENGTH_SHORT).show();
////
////                        ArrayList<String> numbers = new ArrayList<>();
////                        numbers.add("One");
////                        numbers.add("Two");
////                        numbers.add("Three");
////                        numbers.add("Four");
////                        numbers.add("Five");
////
////                        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(getApplicationContext(), numbers);
////                        productList.setAdapter(productDetailsAdapter);
////                        alertDialog.dismiss();
////                    }
////                });
//                submit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        String Rating = "";
//                        String Voltage = "";
//                        String Type = typespin.getText().toString().trim();
//                        if (Type.equals("Please select type")) {
//                            Type = "";
//                            Toast.makeText(ProductListView.this, "Please select type", Toast.LENGTH_LONG).show();
//                        } else {
//                            alertDialog.dismiss();
//                            checkInternet(Rating,Voltage,Type);
//                        }
//                    }
//                });
//
////            alertDialog.getWindow().setLayout(600, 600);
//            }
//        }
//        else
//        {
//            Toast.makeText(getApplicationContext(),"Please check your Network Connection",Toast.LENGTH_SHORT).show();
//        }
    }

    public void checkInternet(String Rating,String Voltage,String Type)
    {
        if(net.CheckInternet())
        {
            getProductdetails(Rating,Voltage,Type);
        }
        else {
            Toast.makeText(this, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    public void getProductdetails(String Rating,String Voltage,String Type)
    {
        final Alertbox alertbox=new Alertbox(ProductListView.this);
        final ProgressDialog progressDialog = new ProgressDialog(ProductListView.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        CategoryAPI service = RetroClient.getApiService();

        try {
            Call<ProductSearch> call = service.productSearch(product, Rating, Type, Voltage);
            call.enqueue(new Callback<ProductSearch>() {
                @Override
                public void onResponse(Call<ProductSearch> call, Response<ProductSearch> response) {
                    if (response.body().getResult().equals("success")) {
                        List<ProductSearchList> Producfilter = response.body().getData();
                        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(ProductListView.this, Producfilter);
                        productList.setAdapter(productDetailsAdapter);
                        progressDialog.dismiss();
                    }else if (response.body().getResult().equals("notsuccess")){
//                        Toast.makeText(getApplicationContext(), "No FeedBackData Found", Toast.LENGTH_LONG).show();
                        alertbox.showNegativebox("No FeedBackData Found");
                        progressDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<ProductSearch> call, Throwable t) {
                    alertbox.showNegativebox("No Record Found");
//                    Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
