package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.OverallSearchAdapter;
import alertbox.Alertbox;
import api.models.overallsearch.OverallSearchData;
import api.models.overallsearch.OverallSearchResult;
import font.FontDesign;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OverallSearch extends AppCompatActivity {

    private ListView overalllist;
    private String searchedstring;
    private OverallSearchAdapter overallSearchAdapter;
    private List<OverallSearchData> data;
    private ProgressDialog loading;
    private FontDesign heading,searchedproduct;
    private ImageView back,product_list_view_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overall_search);
        searchedstring=getIntent().getStringExtra("searchtext");
        overalllist=findViewById(R.id.overalllist);
        heading=findViewById(R.id.heading);
        searchedproduct=findViewById(R.id.searchedproduct);
        back=findViewById(R.id.back);
        product_list_view_home=findViewById(R.id.product_list_view_home);
        heading.setText(searchedstring);
        searchedproduct.setText(searchedstring);
        getoveralldata();

        product_list_view_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(),ProductCatalogue.class);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void getoveralldata() {
        try {
            final Alertbox alertbox=new Alertbox(OverallSearch.this);
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            CategoryAPI service= RetroClient.getApiService();
            Call<OverallSearchResult> call = service.getoverall(searchedstring);
            call.enqueue(new Callback<OverallSearchResult>() {
                @Override
                public void onResponse(Call<OverallSearchResult> call, Response<OverallSearchResult> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            data=response.body().getData();
                            overallSearchAdapter=new OverallSearchAdapter(OverallSearch.this,data,searchedstring);
                            overalllist.setAdapter(overallSearchAdapter);
                        }
                        else {
                            loading.dismiss();
                            alertbox.showAlertbox("No FeedBackData Found");
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox("Expection Error Please Try Again later");
                    }
                }
                @Override
                public void onFailure(Call<OverallSearchResult> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox("Failure Error Please Try Again later");
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
