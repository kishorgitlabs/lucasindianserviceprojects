package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;

import alertbox.Alertbox;
import api.models.checkmaster.Master;
import api.models.checkmaster.Master_data;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import login.Login_Activity;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class ChangePassword_Activity extends AppCompatActivity {


    private MaterialEditText mOLdPassword,mUsername,mNewPassword;
    private Button mSubmit;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private ProgressDialog loading;
    private String mComeFrom="";
    private TextView mLogin_here;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_);
        // Set up the login form.
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        mComeFrom = getIntent().getStringExtra("from");
        mUsername = (MaterialEditText) findViewById(R.id.username);
        mOLdPassword = (MaterialEditText) findViewById(R.id.old_password);
        mNewPassword = (MaterialEditText) findViewById(R.id.password);
        mSubmit = (Button) findViewById(R.id.submit_button);
        mLogin_here = (TextView) findViewById(R.id.login_here);

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(mOLdPassword.getText().length() == 0){
                    toasty.showFailureToast("Enter your old password");
                }else if(mNewPassword.getText().length() == 0){
                    toasty.showFailureToast("Enter your new password");
                }
                else
                    checkInternet();
            }
        });
        mLogin_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnLogin();
            }
        });
        if(mComeFrom.equals("register"))
            mLogin_here.setVisibility(View.VISIBLE);
        else
            mLogin_here.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onBackPressed() {
        if(mComeFrom.equals("register"))
        startActivity(new Intent(ChangePassword_Activity.this, Login_Activity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        else
            super.onBackPressed();

    }

    private void checkInternet() {

        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            checkMobileNumber();
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

    private void checkMobileNumber() {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Master> call = service.ChangePassowrd(myshare.getString("Mobile", ""), mOLdPassword.getText().toString(),mNewPassword.getText().toString());
            call.enqueue(new Callback<Master>() {
                @Override
                public void onResponse(Call<Master> call, Response<Master> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnCheckingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("NotSuccess")) {
                            alertbox.showAlertbox("Old password is invalid");
                        }  else {
                            alertbox.showAlertbox(getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<Master> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OnCheckingSuccess(Master_data data) {
        editor.putBoolean("IsChangePass", true);
        editor.commit();
        startActivity(new Intent(ChangePassword_Activity.this, Login_Activity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

    }

    public void OnLogin() {

        startActivity(new Intent(ChangePassword_Activity.this, Login_Activity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
