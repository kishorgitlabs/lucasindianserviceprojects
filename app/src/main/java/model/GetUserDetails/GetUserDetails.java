
package model.GetUserDetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetUserDetails {

    @SerializedName("data")
    private List<GetUserDetailsList> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetUserDetailsList> getData() {
        return mData;
    }

    public void setData(List<GetUserDetailsList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
