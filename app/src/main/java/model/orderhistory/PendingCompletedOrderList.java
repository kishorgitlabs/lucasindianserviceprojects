
package model.orderhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PendingCompletedOrderList {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("OrderStatus")
    private String mOrderStatus;
    @SerializedName("PartName")
    private String mPartName;
    @SerializedName("PartNumber")
    private String mPartNumber;
    @SerializedName("PartPrice")
    private Long mPartPrice;
    @SerializedName("Quantity")
    private Long mQuantity;

    public String getmDispatchedqty() {
        return mDispatchedqty;
    }

    public void setmDispatchedqty(String mDispatchedqty) {
        this.mDispatchedqty = mDispatchedqty;
    }

    @SerializedName("DispatchedQty")
    private String mDispatchedqty;


    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        mOrderStatus = orderStatus;
    }

    public String getPartName() {
        return mPartName;
    }

    public void setPartName(String partName) {
        mPartName = partName;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public Long getPartPrice() {
        return mPartPrice;
    }

    public void setPartPrice(Long partPrice) {
        mPartPrice = partPrice;
    }

    public Long getQuantity() {
        return mQuantity;
    }

    public void setQuantity(Long quantity) {
        mQuantity = quantity;
    }

}
