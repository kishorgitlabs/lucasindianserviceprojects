
package model.productsearch;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductType {

    @SerializedName("data")
    private ProductTypeList mData;
    @SerializedName("result")
    private String mResult;

    public ProductTypeList getData() {
        return mData;
    }

    public void setData(ProductTypeList data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
