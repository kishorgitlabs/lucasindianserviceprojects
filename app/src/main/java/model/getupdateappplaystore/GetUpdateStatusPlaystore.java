
package model.getupdateappplaystore;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetUpdateStatusPlaystore {

    @SerializedName("data")
    private String mData;
    @SerializedName("prmtdata")
    private Object mPrmtdata;
    @SerializedName("result")
    private String mResult;

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }

    public Object getPrmtdata() {
        return mPrmtdata;
    }

    public void setPrmtdata(Object prmtdata) {
        mPrmtdata = prmtdata;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
