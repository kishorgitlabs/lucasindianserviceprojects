
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartNoSearch {

    @SerializedName("data")
    private List<PartNoListDetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<PartNoListDetails> getData() {
        return mData;
    }

    public void setData(List<PartNoListDetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
