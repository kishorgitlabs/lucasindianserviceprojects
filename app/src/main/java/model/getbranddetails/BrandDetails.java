
package model.getbranddetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class BrandDetails {

    @SerializedName("data")
    private List<BrandData> mData;
    @SerializedName("result")
    private String mResult;

    public List<BrandData> getData() {
        return mData;
    }

    public void setData(List<BrandData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
