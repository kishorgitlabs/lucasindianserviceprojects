package model;

public class Order_Parts {
    String partNumber,Description,partImage,partName,Quantity,PartPrice,TotalAmount,DeliveryStatus,Active;

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDescription() {
        return Description;
    }

    public void setPartDescription(String partDescription) {
        this.Description = partDescription;
    }

    public String getPartImage() {
        return partImage;
    }

    public void setPartImage(String partImage) {
        this.partImage = partImage;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartQuantity() {
        return Quantity;
    }

    public void setPartQuantity(String partQuantity) {
        this.Quantity = partQuantity;
    }

    public String getPartPrice() {
        return PartPrice;
    }

    public void setPartPrice(String partPrice) {
        PartPrice = partPrice;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        DeliveryStatus = deliveryStatus;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

}
