
package model.PartSearch;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ServiceParts {

    @SerializedName("data")
    private List<ServicePartsList> mData;
    @SerializedName("result")
    private String mResult;

    public List<ServicePartsList> getData() {
        return mData;
    }

    public void setData(List<ServicePartsList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
