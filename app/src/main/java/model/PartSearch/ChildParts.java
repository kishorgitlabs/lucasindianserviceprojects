
package model.PartSearch;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChildParts {

    @SerializedName("data")
    private List<ChildPartsList> mData;
    @SerializedName("result")
    private String mResult;

    public List<ChildPartsList> getData() {
        return mData;
    }

    public void setData(List<ChildPartsList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
