
package model.PartSearch;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ChildPartsList implements Serializable{

    @SerializedName("C_Id")
    private Long mCId;
    @SerializedName("childparts")
    private String mChildparts;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("illno")
    private String mIllno;
    @SerializedName("illno_sno")
    private String mIllnoSno;
    @SerializedName("InsertDate")
    private Object mInsertDate;
    @SerializedName("manufacturername")
    private String mManufacturername;
    @SerializedName("moq")
    private String mMoq;
    @SerializedName("mrp")
    private String mMrp;
    @SerializedName("partno")
    private String mPartno;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("UpdateDate")
    private Object mUpdateDate;

    public Long getCId() {
        return mCId;
    }

    public void setCId(Long cId) {
        mCId = cId;
    }

    public String getChildparts() {
        return mChildparts;
    }

    public void setChildparts(String childparts) {
        mChildparts = childparts;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getIllno() {
        return mIllno;
    }

    public void setIllno(String illno) {
        mIllno = illno;
    }

    public String getIllnoSno() {
        return mIllnoSno;
    }

    public void setIllnoSno(String illnoSno) {
        mIllnoSno = illnoSno;
    }

    public Object getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(Object insertDate) {
        mInsertDate = insertDate;
    }

    public String getManufacturername() {
        return mManufacturername;
    }

    public void setManufacturername(String manufacturername) {
        mManufacturername = manufacturername;
    }

    public String getMoq() {
        return mMoq;
    }

    public void setMoq(String moq) {
        mMoq = moq;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String mrp) {
        mMrp = mrp;
    }

    public String getPartno() {
        return mPartno;
    }

    public void setPartno(String partno) {
        mPartno = partno;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object status) {
        mStatus = status;
    }

    public Object getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(Object updateDate) {
        mUpdateDate = updateDate;
    }

}
