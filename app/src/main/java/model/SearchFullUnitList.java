
package model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SearchFullUnitList {

    @SerializedName("Ser_Childpartno")
    private String mSerChildpartno;
    @SerializedName("Ser_Desc")
    private String mSerDesc;
    @SerializedName("Ser_ilno")
    private String mSerIlno;
    @SerializedName("Ser_noff")
    private String mSerNoff;
    @SerializedName("Ser_parno")
    private String mSerParno;
    @SerializedName("Ser_proimage")
    private String mSerProimage;
    @SerializedName("sid")
    private Long mSid;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("vhid")
    private Long mVhid;
    @SerializedName("warid")
    private Long mWarid;

    public String getSerChildpartno() {
        return mSerChildpartno;
    }

    public void setSerChildpartno(String serChildpartno) {
        mSerChildpartno = serChildpartno;
    }

    public String getSerDesc() {
        return mSerDesc;
    }

    public void setSerDesc(String serDesc) {
        mSerDesc = serDesc;
    }

    public String getSerIlno() {
        return mSerIlno;
    }

    public void setSerIlno(String serIlno) {
        mSerIlno = serIlno;
    }

    public String getSerNoff() {
        return mSerNoff;
    }

    public void setSerNoff(String serNoff) {
        mSerNoff = serNoff;
    }

    public String getSerParno() {
        return mSerParno;
    }

    public void setSerParno(String serParno) {
        mSerParno = serParno;
    }

    public String getSerProimage() {
        return mSerProimage;
    }

    public void setSerProimage(String serProimage) {
        mSerProimage = serProimage;
    }

    public Long getSid() {
        return mSid;
    }

    public void setSid(Long sid) {
        mSid = sid;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object status) {
        mStatus = status;
    }

    public Long getVhid() {
        return mVhid;
    }

    public void setVhid(Long vhid) {
        mVhid = vhid;
    }

    public Long getWarid() {
        return mWarid;
    }

    public void setWarid(Long warid) {
        mWarid = warid;
    }

}
