package model;

import java.io.Serializable;

public class PartModelClass implements Serializable {
    String partNo;
    String appName;
    String description;
    String oemPartNo;
    String proStatus;
    String proType;
    String productName;
    String partVolt;
    String partOutputrng;
    String proModel;
    String proSupersed;
    String Quantity;
    String Partimage;

    public String getPartimage() {
        return Partimage;
    }

    public void setPartimage(String partimage) {
        Partimage = partimage;
    }

    String mrp;

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOemPartNo() {
        return oemPartNo;
    }

    public void setOemPartNo(String oemPartNo) {
        this.oemPartNo = oemPartNo;
    }

    public String getProStatus() {
        return proStatus;
    }

    public void setProStatus(String proStatus) {
        this.proStatus = proStatus;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPartVolt() {
        return partVolt;
    }

    public void setPartVolt(String partVolt) {
        this.partVolt = partVolt;
    }

    public String getPartOutputrng() {
        return partOutputrng;
    }

    public void setPartOutputrng(String partOutputrng) {
        this.partOutputrng = partOutputrng;
    }

    public String getProModel() {
        return proModel;
    }

    public void setProModel(String proModel) {
        this.proModel = proModel;
    }

    public String getProSupersed() {
        return proSupersed;
    }

    public void setProSupersed(String proSupersed) {
        this.proSupersed = proSupersed;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }


}
