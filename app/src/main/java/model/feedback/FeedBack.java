
package model.feedback;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class FeedBack {

    @SerializedName("data")
    private FeedBackData mData;
    @SerializedName("result")
    private String mResult;

    public FeedBackData getData() {
        return mData;
    }

    public void setData(FeedBackData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
