
package model.giftsumma;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GiftCode {

    @SerializedName("gcode")
    private String gcode;

    public String getGcode() {
        return gcode;
    }

    public void setGcode(String gcode) {
        this.gcode = gcode;
    }

}
