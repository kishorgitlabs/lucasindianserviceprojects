package orders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.Collections;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.CompletedAdapter;
import home.Home_Activity;
import model.orderhistory.CompletedOrderList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class CompletedOrders extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_orders);
        ImageView cart=findViewById(R.id.completed_order_table_cart);
        ImageView home=findViewById(R.id.completed_home);
        ImageView back=findViewById(R.id.completed_back);
        listView=findViewById(R.id.completed_order_list);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CompletedOrders.this,OrderActivity.class));
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CompletedOrders.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        SharedPreferences myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        final String dlr=(myshare.getString("UserCode","1"));
        final String userType=myshare.getString("UserType","1");


        NetworkConnection networkConnection=new NetworkConnection(CompletedOrders.this);
        if(networkConnection.CheckInternet())
                    getCompletedOrder(userType,dlr);
//            getCompletedOrder("DLR",7777);
        else {
            StyleableToasty styleableToasty = new StyleableToasty(CompletedOrders.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }

    }

    private void getCompletedOrder(String userType, String tempDlr)
    {
        String tempUserType=userType;
        if(!tempUserType.equals("LISSO") && !userType.equals("MSR"))
        {
            final ProgressDialog progressDialog = new ProgressDialog(CompletedOrders.this, R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            try {
                CategoryAPI service = RetroClient.getApiService();
//                String dlr = String.valueOf(tempDlr);
                Call<model.orderhistory.CompletedOrders> call = service.searchCompletedNonLMUser(tempDlr, userType);

                call.enqueue(new Callback<model.orderhistory.CompletedOrders>() {
                    @Override
                    public void onResponse(Call<model.orderhistory.CompletedOrders> call, Response<model.orderhistory.CompletedOrders> response) {
                        progressDialog.dismiss();
                        if(response.body().getResult().equals("Success"))
                        {
                            viewAdapter(response.body().getData());
                        }
                        else {
                            StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                            styleableToasty.showSuccessToast("Failed to find the Order");
                        }
                    }

                    @Override
                    public void onFailure(Call<model.orderhistory.CompletedOrders> call, Throwable t) {
                        progressDialog.dismiss();
                        StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                        styleableToasty.showSuccessToast("Some thing went wrong please try again later");
                    }
                });
            }
            catch (Exception e)
            {
                progressDialog.dismiss();
                e.printStackTrace();
                StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                styleableToasty.showSuccessToast("Server Problem Please try again later");
            }
        }
        else {
            final ProgressDialog progressDialog = new ProgressDialog(CompletedOrders.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            try {
                CategoryAPI service = RetroClient.getApiService();
//                String dlr = String.valueOf(tempDlr);
                Call<model.orderhistory.CompletedOrders> call = service.searchCompletedLMUSer(tempDlr, userType);

                call.enqueue(new Callback<model.orderhistory.CompletedOrders>() {
                    @Override
                    public void onResponse(Call<model.orderhistory.CompletedOrders> call, Response<model.orderhistory.CompletedOrders> response) {
                        progressDialog.dismiss();
                        if(response.body().getResult().equals("Success"))
                        viewAdapter(response.body().getData());
                        else {
                            StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                            styleableToasty.showSuccessToast("Failed to find the Order");
                        }
                    }

                    @Override
                    public void onFailure(Call<model.orderhistory.CompletedOrders> call, Throwable t) {
                        progressDialog.dismiss();
                        StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                        styleableToasty.showSuccessToast("Some thing went wrong please try again later");
                    }
                });
            }
            catch (Exception e)
            {
                progressDialog.dismiss();
                e.printStackTrace();
                StyleableToasty styleableToasty=new StyleableToasty(CompletedOrders.this);
                styleableToasty.showSuccessToast("Server Problem Please try again later");
            }
        }

    }
    private void viewAdapter(List<CompletedOrderList> data)
    {
        listView.setAdapter(new CompletedAdapter(CompletedOrders.this,data));
    }
}
