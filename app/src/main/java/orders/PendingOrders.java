package orders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.PendingAdapter;
import home.Home_Activity;
import model.orderhistory.PendingOrderList;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class PendingOrders extends AppCompatActivity {
    private ListView pendingOrderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);
        ImageView back=findViewById(R.id.pending_back);
        ImageView home=findViewById(R.id.pending_home);
        ImageView cart=findViewById(R.id.pending_order_table_cart);
        pendingOrderList=findViewById(R.id.pending_order_list);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PendingOrders.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PendingOrders.this,OrderActivity.class));
            }
        });
        SharedPreferences myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        final String userType=myshare.getString("UserType","1");
        final String dlr=(myshare.getString("UserCode","1"));

//        final String mobileNo=myshare.getString("Mobile","1");
//        final String userName=myshare.getString("UserName","1");
//        final String userAddress=myshare.getString("UserAddress","");
        NetworkConnection networkConnection=new NetworkConnection(PendingOrders.this);
        if(networkConnection.CheckInternet())
        getPendingOrders(userType,dlr);
        else {
            StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
            styleableToasty.showSuccessToast("Please check Your Internet Connection");
        }
    }

    public void getPendingOrders(String userType, String tempDlr)
    {

        String tempUserType=userType;
        if(!tempUserType.equals("LISSO") && !userType.equals("MSR"))
        {
            final ProgressDialog progressDialog = new ProgressDialog(PendingOrders.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            try {
                CategoryAPI service = RetroClient.getApiService();
//                String dlr = String.valueOf(tempDlr);
                Call<model.orderhistory.PendingOrders> call = service.searchPendingNonLMUser(tempDlr, userType);
                call.enqueue(new Callback<model.orderhistory.PendingOrders>() {
                    @Override
                    public void onResponse(Call<model.orderhistory.PendingOrders> call, Response<model.orderhistory.PendingOrders> response) {
                        progressDialog.dismiss();
                        if(response.body().getResult().equals("Success"))
                        {
//                            StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
//                            styleableToasty.showSuccessToast("Success");
                            viewAdapter(response.body().getData());
                        }
                        else
                        {
                            StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                            styleableToasty.showSuccessToast("Failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<model.orderhistory.PendingOrders> call, Throwable t) {
                        progressDialog.dismiss();
                        StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                        styleableToasty.showSuccessToast("Some thing went wrong please try again later");
                    }
                });
            }
            catch (Exception e)
            {
                progressDialog.dismiss();
                e.printStackTrace();
                StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                styleableToasty.showSuccessToast("Server Problem Please try again later");
            }


        }
        else {
            final ProgressDialog progressDialogoDup = new ProgressDialog(PendingOrders.this,
                    R.style.Progress);
            progressDialogoDup.setIndeterminate(true);
            progressDialogoDup.setMessage("Loading...");
            progressDialogoDup.setCancelable(false);

            progressDialogoDup.show();
            try {

                CategoryAPI service = RetroClient.getApiService();
//                String dlr = String.valueOf(tempDlr);
                Call<model.orderhistory.PendingOrders> call = service.searchPendingLMUSer(tempDlr, userType);
                call.enqueue(new Callback<model.orderhistory.PendingOrders>() {
                    @Override
                    public void onResponse(Call<model.orderhistory.PendingOrders> call, Response<model.orderhistory.PendingOrders> response) {
                        progressDialogoDup.dismiss();
                        if(response.body().getResult().equals("Success"))
                        {
//                            StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
//                            styleableToasty.showSuccessToast("Success");
                            viewAdapter(response.body().getData());
                        }
                        else
                        {
                            StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                            styleableToasty.showSuccessToast("Failed");
                        }
                    }

                    @Override
                    public void onFailure(Call<model.orderhistory.PendingOrders> call, Throwable t) {
                        progressDialogoDup.dismiss();
                        StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                        styleableToasty.showSuccessToast("Some thing went wrong please try again later");
                    }
                });
            }
            catch (Exception e)
            {
                progressDialogoDup.dismiss();
                StyleableToasty styleableToasty=new StyleableToasty(PendingOrders.this);
                styleableToasty.showSuccessToast("Server Problem Please try again later");
            }
        }
    }

    public void viewAdapter(List<PendingOrderList> data)
    {
        PendingAdapter pendingAdapter=new PendingAdapter(PendingOrders.this,data);
        pendingOrderList.setAdapter(pendingAdapter);

    }
}
