package date;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;


public class  PickFromDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public SendFromDate sm;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        int year = c.get(Calendar.YEAR);

        int month = c.get(Calendar.MONTH);

        int day = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        Long d = 7776000000L;
//            long milliSec=System.currentTimeMillis();
//            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - d);


//            datePickerDialog.getDatePicker().setMaxDate(90);


        // Create a new instance of DatePickerDialog and return it

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        String mFromdate=year+"-"+(month+1)+"-"+day;
        sm.sendMessageFromDate(mFromdate);

    }


    public interface SendFromDate{
        void sendMessageFromDate(String message);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            sm = (SendFromDate) getActivity();
        }catch (Exception e)
        {

        }
    }
}



