package api.models.checkmaster;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Awesome Pojo Generator
 */
public class Master_data implements Serializable {
    @SerializedName("MobileNo")
    @Expose
    private String MobileNo;
    @SerializedName("Designation")
    @Expose
    private String Designation;
    @SerializedName("DLR_code")
    @Expose
    private String DLR_code;
    @SerializedName("Updatedate")
    @Expose
    private String Updatedate;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("Address")
    @Expose
    private String Address;
    @SerializedName("Insertdate")
    @Expose
    private String Insertdate;
    @SerializedName("Membership_no")
    @Expose
    private String Membership_no;
    @SerializedName("SalesOfficerCode")
    @Expose
    private String SalesOfficerCode;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("EmployeeId")
    @Expose
    private String EmployeeId;
    @SerializedName("UserType")
    @Expose
    private String UserType;
    @SerializedName("IMEI_number")
    @Expose
    private String IMEI_number;

    @SerializedName("ModelName")
    @Expose
    private String ModelName;

    @SerializedName("ModelNumber")
    @Expose
    private String ModelNumber;


    public void setMobileNo(String MobileNo) {
        this.MobileNo = MobileNo;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDLR_code(String DLR_code) {
        this.DLR_code = DLR_code;
    }

    public String getDLR_code() {
        return DLR_code;
    }

    public void setUpdatedate(String Updatedate) {
        this.Updatedate = Updatedate;
    }

    public String getUpdatedate() {
        return Updatedate;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getEmail() {
        return Email;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getAddress() {
        return Address;
    }

    public void setInsertdate(String Insertdate) {
        this.Insertdate = Insertdate;
    }

    public String getInsertdate() {
        return Insertdate;
    }

    public void setMembership_no(String Membership_no) {
        this.Membership_no = Membership_no;
    }

    public String getMembership_no() {
        return Membership_no;
    }

    public void setSalesOfficerCode(String SalesOfficerCode) {
        this.SalesOfficerCode = SalesOfficerCode;
    }

    public String getSalesOfficerCode() {
        return SalesOfficerCode;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEmployeeId(String EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getEmployeeId() {
        return EmployeeId;
    }

    public void setUserType(String UserType) {
        this.UserType = UserType;
    }

    public String getUserType() {
        return UserType;
    }

    public void setIMEI_number(String IMEI_number) {
        this.IMEI_number = IMEI_number;
    }

    public String getIMEI_number() {
        return IMEI_number;
    }

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    public String getModelNumber() {
        return ModelNumber;
    }

    public void setModelNumber(String modelNumber) {
        ModelNumber = modelNumber;
    }
}