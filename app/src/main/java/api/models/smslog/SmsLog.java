package api.models.smslog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class SmsLog{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private SmsData data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(SmsData data){
   this.data=data;
  }
  public SmsData getData(){
   return data;
  }
}