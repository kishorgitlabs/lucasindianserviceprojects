
package api.models.scanhistory.detailpoints;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class GetRMDetailsCode {

    @SerializedName("data")
    private List<GetRmDetailsPointsList> mData;
    @SerializedName("result")
    private String mResult;

    public List<GetRmDetailsPointsList> getData() {
        return mData;
    }

    public void setData(List<GetRmDetailsPointsList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
