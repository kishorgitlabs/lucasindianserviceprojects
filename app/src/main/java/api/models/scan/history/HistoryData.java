package api.models.scan.history;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class HistoryData {
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("JoReCode")
  @Expose
  private String JoReCode;
  @SerializedName("PartNumber")
  @Expose
  private String PartNumber;
  @SerializedName("ScannerBy")
  @Expose
  private String ScannerBy;
  @SerializedName("Code")
  @Expose
  private String Code;
  @SerializedName("Date")
  @Expose
  private String Date;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("Points")
  @Expose
  private String Points;
  @SerializedName("SerialNumber")
  @Expose
  private String SerialNumber;
  @SerializedName("ScannerFor")
  @Expose
  private String ScannerFor;
  @SerializedName("SmsFrom")
  @Expose
  private String SmsFrom;
  @SerializedName("SapSms")
  @Expose
  private String SapSms;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("GenCode")
  @Expose
  private String GenCode;
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setJoReCode(String JoReCode){
   this.JoReCode=JoReCode;
  }
  public String getJoReCode(){
   return JoReCode;
  }
  public void setPartNumber(String PartNumber){
   this.PartNumber=PartNumber;
  }
  public String getPartNumber(){
   return PartNumber;
  }
  public void setScannerBy(String ScannerBy){
   this.ScannerBy=ScannerBy;
  }
  public String getScannerBy(){
   return ScannerBy;
  }
  public void setCode(String Code){
   this.Code=Code;
  }
  public String getCode(){
   return Code;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setPoints(String Points){
   this.Points=Points;
  }
  public String getPoints(){
   return Points;
  }
  public void setSerialNumber(String SerialNumber){
   this.SerialNumber=SerialNumber;
  }
  public String getSerialNumber(){
   return SerialNumber;
  }
  public void setScannerFor(String ScannerFor){
   this.ScannerFor=ScannerFor;
  }
  public String getScannerFor(){
   return ScannerFor;
  }
  public void setSmsFrom(String SmsFrom){
   this.SmsFrom=SmsFrom;
  }
  public String getSmsFrom(){
   return SmsFrom;
  }
  public void setSapSms(String SapSms){
   this.SapSms=SapSms;
  }
  public String getSapSms(){
   return SapSms;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setGenCode(String GenCode){
   this.GenCode=GenCode;
  }
  public String getGenCode(){
   return GenCode;
  }
}