package api.models.partsdata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Parts_Data {
  @SerializedName("GcCode")
  @Expose
  private String GcCode;
  @SerializedName("MaterialDesc")
  @Expose
  private String MaterialDesc;
  @SerializedName("Points")
  @Expose
  private String Points;
  @SerializedName("Usertype")
  @Expose
  private String Usertype;
  @SerializedName("InsertDate")
  @Expose
  private String InsertDate;
  @SerializedName("Material")
  @Expose
  private String Material;
  @SerializedName("id")
  @Expose
  private Integer id;
  public void setGcCode(String GcCode){
   this.GcCode=GcCode;
  }
  public String getGcCode(){
   return GcCode;
  }
  public void setMaterialDesc(String MaterialDesc){
   this.MaterialDesc=MaterialDesc;
  }
  public String getMaterialDesc(){
   return MaterialDesc;
  }
  public void setPoints(String Points){
   this.Points=Points;
  }
  public String getPoints(){
   return Points;
  }
  public void setUsertype(String Usertype){
   this.Usertype=Usertype;
  }
  public String getUsertype(){
   return Usertype;
  }
  public void setInsertDate(String InsertDate){
   this.InsertDate=InsertDate;
  }
  public String getInsertDate(){
   return InsertDate;
  }
  public void setMaterial(String Material){
   this.Material=Material;
  }
  public String getMaterial(){
   return Material;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
}