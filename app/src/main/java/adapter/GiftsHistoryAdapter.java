package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import java.lang.reflect.Array;
import java.util.List;

import api.models.gifthistory.GetHistoryData;
import giftcatalogue.GiftsHistory;
import home.Home_Activity;
import toast.StyleableToasty;

public class GiftsHistoryAdapter extends ArrayAdapter {
    private Context context;
    private int datas;
    private List<GetHistoryData> data;

    public GiftsHistoryAdapter(@NonNull Context context,List<GetHistoryData> data ) {
        super(context, R.layout.gift_history_adapter);
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.gift_history_adapter, parent, false);

             TextView snogiftadapter=view.findViewById(R.id.snogiftadapter);
             TextView productcode=view.findViewById(R.id.productcode);
             TextView points=view.findViewById(R.id.points);
             TextView productname=view.findViewById(R.id.productname);
             TextView redeemdate=view.findViewById(R.id.redeemdate);

             snogiftadapter.setText(position+1+"");
             productcode.setText(data.get(position).getPRODUCTCODE());
             points.setText(data.get(position).getPoints());
             productname.setText(data.get(position).getPRODUCTNAME());

             String dateget=data.get(position).getRDATE();

             String[] date=dateget.split("-");
                String dateformat=date[2]+"/"+date[1]+"/"+date[0];
             redeemdate.setText(dateformat);


        return view;
    }

    @Override
    public int getCount () {
        return data.size();
    }
}