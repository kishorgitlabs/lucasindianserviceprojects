package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import java.util.HashSet;
import java.util.List;



public class ViewaddressAdapter extends ArrayAdapter {

    private Context context;
    private List<String> add;

    public ViewaddressAdapter(@NonNull Context context, List<String> data) {
        super(context, R.layout.viewaddressadapter);
        this.context=context;
        this.add=data;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView==null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.viewaddressadapter, null);

            TextView viewaddressview=convertView.findViewById(R.id.viewaddressview);
//            HashSet<String> addresss=new HashSet<>();
//
//            addresss.add(add.get(position).getNewAddress());

            viewaddressview.setText(add.get(position));


        }
        return convertView;
    }

    @Override
    public int getCount() {
        return add.size();
    }
}
