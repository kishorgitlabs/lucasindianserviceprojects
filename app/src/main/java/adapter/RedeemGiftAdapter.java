package adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alertbox.Alertbox;
import model.giftcatalogue.redeempoints.JodidarRedeemPoints;
import model.giftcatalogue.redeempoints.Redeem;
import model.giftsumma.GiftCode;
import toast.StyleableToasty;

public class RedeemGiftAdapter extends ArrayAdapter {
    private List<String> adapterList=new ArrayList<>();
    private Context context;
    private ClickInterface clickInterface;
    private JodidarRedeemPoints points;
    private int redeemablePoints;
    private String availablePoints;
    private int temp=0;
    private Map<Integer,Boolean> isCheckedMap;
    private List<GiftCode> giftCodeList;
//    private List<Redeem> redeems;
    private List<String> giftcodes;
    private List<String> giftname;
    private List<String> giftpoints;
    private Alertbox alertbox;
    private HashMap<String,GiftCode> hashMap=new HashMap<>();

    private HashMap<String,Redeem> preview =new HashMap<>();

    public RedeemGiftAdapter(@NonNull Context context, JodidarRedeemPoints points, String availablePoints, int redeemablePoints) {
        super(context, R.layout.gift_adapter);
        giftCodeList=new ArrayList<>();
        giftcodes=new ArrayList<>();
        giftname=new ArrayList<>();
        giftpoints=new ArrayList<>();
//        redeems=new ArrayList<>();
        this.context=context;
        this.points=points;
        this.availablePoints=availablePoints;
        this.redeemablePoints=redeemablePoints;
        isCheckedMap=new HashMap<>();
        alertbox=new Alertbox(context);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view=LayoutInflater.from(context).inflate(R.layout.gift_adapter,parent,false);

//        redeems=new ArrayList<>();
        clickInterface= (ClickInterface) context;
        TextView sno=view.findViewById(R.id.redeem_sno);
        TextView desc=view.findViewById(R.id.redeem_product_desc);
        final TextView product=view.findViewById(R.id.redeem_product_code);
        final TextView point=view.findViewById(R.id.redeem_points);
        LinearLayout checkLayout=view.findViewById(R.id.checkBox_layout);
        CheckBox checkBox=view.findViewById(R.id.redeem_check_box);
        sno.setText(position+1+"");

        desc.setText(points.getData().getRedeem().get(position).getName());
        product.setText(points.getData().getRedeem().get(position).getGiftCode());
        point.setText(points.getData().getRedeem().get(position).getPoints());


        //
        if(Integer.parseInt(points.getData().getRedeem().get(position).getPoints())>redeemablePoints)
        {
            if(isCheckedMap.containsKey(position))
            {
                checkBox.setChecked(isCheckedMap.get(position));
//                setCheckBoxChecked(checkBox,position);
            }
//            else {
//                checkBox.setEnabled(false);
//            }
        }
        else {
//            checkBox.setEnabled(true);
            if(isCheckedMap.containsKey(position))
            {
                checkBox.setChecked(isCheckedMap.get(position));
//                setCheckBoxChecked(checkBox,position);
            }
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Redeem redeem =new Redeem();
                if(isChecked){

                    if(Integer.parseInt(points.getData().getRedeem().get(position).getPoints())>redeemablePoints){
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Your Redeemable Points is" +" "+ +redeemablePoints+ ".\n"+ "So Select within Your Redeemable points");

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
//                        builder.setView(builder);
                        builder.setCancelable(false);
                        builder.show();
//                        StyleableToasty toasty=new StyleableToasty(context);
//                        toasty.showSuccessToast("Points Reached");
                        buttonView.setChecked(false);

                    }

//                   else if(Integer.parseInt(points.getData().getRedeem().get(position).getPoints())>redeemablePoints)
//                    {
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                        builder.setMessage("Your Redeemable Gift Points Reached");
//                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                            }
//                        });
////                        builder.setView(builder);
//                        builder.setCancelable(false);
//                        builder.show();
////                        StyleableToasty toasty=new StyleableToasty(context);
////                        toasty.showSuccessToast("Points Reached");
//                        buttonView.setChecked(false);
//                    }

                    else {
                        GiftCode giftCode=new GiftCode();
                        giftCode.setGcode(points.getData().getRedeem().get(position).getGiftCode());
                        hashMap.put(points.getData().getRedeem().get(position).getGiftCode(),giftCode);
                        giftCodeList.add(giftCode);

                        giftcodes.add(points.getData().getRedeem().get(position).getGiftCode());
                        giftname.add(points.getData().getRedeem().get(position).getName());
                        giftpoints.add(points.getData().getRedeem().get(position).getPoints());

//                        redeems.add(redeem);
                        preview.put(points.getData().getRedeem().get(position).getGiftCode(),redeem);

                        adapterList.add(points.getData().getRedeem().get(position).getGiftCode());
                        redeemablePoints-=Integer.parseInt(points.getData().getRedeem().get(position).getPoints());;
                        isCheckedMap.put(position,isChecked);
                        temp+=Integer.parseInt(points.getData().getRedeem().get(position).getPoints());
                    }
                }
                else {
                    adapterList.remove(points.getData().getRedeem().get(position).getGiftCode());
                    giftCodeList.remove(hashMap.get(points.getData().getRedeem().get(position).getGiftCode()));

                    giftcodes.remove(points.getData().getRedeem().get(position).getGiftCode());
                    giftname.remove(points.getData().getRedeem().get(position).getName());
                    giftpoints.remove(points.getData().getRedeem().get(position).getPoints());

                    redeemablePoints+=Integer.parseInt(points.getData().getRedeem().get(position).getPoints());
                    isCheckedMap.remove(position);
                    temp-=Integer.parseInt(points.getData().getRedeem().get(position).getPoints());
                }
                clickInterface.onCheckedListener(giftCodeList,temp,isChecked,giftcodes,giftname,giftpoints);
//                notifyDataSetChanged();
            }
        });
        return view;
    }

    @Override
    public int getCount() {
        return points.getData().getRedeem().size();
    }

    public interface ClickInterface {

        void onCheckedListener(List<GiftCode> list,int points,boolean isChecked,List<String> gcode,List<String> gname,List<String> gpoints);
    }


}
