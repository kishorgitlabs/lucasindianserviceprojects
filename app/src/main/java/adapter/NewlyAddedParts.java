package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import java.util.HashMap;
import java.util.List;

public class NewlyAddedParts extends ArrayAdapter {

    private Context context;
    private HashMap<String,String> partsMap;
    private List<String> partNumbers;

    public NewlyAddedParts(@NonNull Context context, HashMap<String,String> partsMap,List<String> partNumbers) {
        super(context, R.layout.newly_added_parts_adapter);
        this.context=context;
        this.partsMap=partsMap;
        this.partNumbers=partNumbers;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view=null;
        try {
            view = LayoutInflater.from(context).inflate(R.layout.newly_added_parts_adapter, null);
            TextView sNo = view.findViewById(R.id.new_sno);
            TextView parts = view.findViewById(R.id.new_parts);
            TextView quantity = view.findViewById(R.id.new_quantity);

            sNo.setText(position + 1 + "");
            parts.setText(partNumbers.get(position));
            quantity.setText(partsMap.get(partNumbers.get(position)));
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public int getCount() {
        return partNumbers.size();
    }
}
