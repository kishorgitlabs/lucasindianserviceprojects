package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SegmentAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<String> list;
    private ArrayList<String> imageId;

//    private Integer[] imageId = {R.drawable.twowheeler, R.drawable.threewheeler, R.drawable.passangercar, R.drawable.utilityvehicle,
//            R.drawable.lightcommercial, R.drawable.heavycommercial, R.drawable.tractors,
//            R.drawable.gensets, R.drawable.engines, R.drawable.marine, R.drawable.defense, R.drawable.locomotive};

    public SegmentAdapter(Context context, ArrayList<String> list,ArrayList<String> imageId)
    {
        super(context, R.layout.segment_list_adapter);
        this.context=context;
        this.list=list;
        this.imageId=imageId;
    }

    //set the adapter size
    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.segment_list_adapter,null);
            ImageView segmentView=convertView.findViewById(R.id.grid_image);
            TextView segmentName=convertView.findViewById(R.id.grid_text);

            segmentName.setText(""+list.get(position));
                segmentView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            segmentView.setImageResource(imageId[position]);
            Picasso.with(context).load(imageId.get(position)).into(segmentView);

        }
        return convertView;
    }
}
